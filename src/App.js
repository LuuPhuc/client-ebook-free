import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from 'history';

//Pages
import LoginPage from 'views/LoginPage/LoginPage';
import HomePage from 'views/HomePage/HomePage';

import "assets/scss/material-kit-react.scss?v=1.8.0";

var hist = createBrowserHistory();

export default function App({ store }) {
    return (
        <Provider store={store}>
            <BrowserRouter history={hist}>
                <Switch>
                    {/* <Route path='/login' exact render={(props) => {
                            if (isAuthenticated) return <Redirect to='/' />
                            // props: {location, match, history}
                            return <LoginPage {...props} />
                        }}
                    /> */}

                    <Route path='/' exact component={HomePage} />
                    <Route path='/login' component={LoginPage} />
                </Switch>
            </BrowserRouter>
        </Provider>
    )
}

// import React from "react";
// import ReactDOM from "react-dom";
// import { createBrowserHistory } from "history";
// import { Router, Route, Switch } from "react-router-dom";


// // pages for this product
// import Components from "views/Components/Components.js";
// import LandingPage from "views/LandingPage/LandingPage.js";
// import ProfilePage from "views/ProfilePage/ProfilePage.js";


// ReactDOM.render(
//     <Router history={hist}>
//         <Switch>
//             <Route path="/landing-page" component={LandingPage} />
//             <Route path="/profile-page" component={ProfilePage} />
//             <Route path="/login-page" component={LoginPage} />
//             <Route path="/" component={Components} />
//         </Switch>
//     </Router>,
//     document.getElementById("root")
// );
