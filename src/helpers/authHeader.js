export function getAuthToken() {
    // NOTE: return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.authToken) {
        return 'Bearer ' + user.authToken;
    } else return '';
}
