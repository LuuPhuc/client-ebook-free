import { call, put } from 'redux-saga/effects';

import AuthService from '../services/auth.service';
import AuthActions from '../store/ducks/auth.duck';
import setHeader from '../utils/setHeader';

import jwtDecode from 'jwt-decode';

const api = new AuthService();

export function* login(action) {
    const { requestType, params } = action;
    try {
        let result = yield call(api.loginInternal, params);
        if (result) {
            yield put(AuthActions.loginSuccess(requestType, result));
            const decoded = jwtDecode(result.token);
            if (decoded.role === 'admin') return Promise.reject({ message: 'You do not have access' })
        }
        throw result;
    } catch (e) {
        yield put(AuthActions.commonAuthFailure(requestType, e));
    }
}