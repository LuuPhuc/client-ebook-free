import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import { TextField } from '@material-ui/core';
import { Formik } from 'formik';
// redux
import { connect } from 'react-redux';
import authActions from '../../store/ducks/auth.duck';

import styles from "assets/jss/material-kit-react/views/loginPage.js";
import image from "assets/img/bg7.jpg";
// import { render } from "node-sass";

const useStyles = makeStyles(styles);

function LoginPage(props) {
    //Default of kit
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    setTimeout(function () {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();
    // const { ...rest } = props;
    //Handle submit
    const onSubmit = (values, { setStatus, setSubmitting }) => {
        props.login('login', values);
    }
    //Styles for material-UI core
    const useStylesCore = makeStyles((theme) => ({
        root: {
            '& > *': {
                margin: theme.spacing(1),
                width: '30ch',
            },
        },
    }));
    const classesRoot = useStylesCore();
    return (
        <div>
            <Header
                absolute
                color="transparent"
                brand="Material Kit React"
                rightLinks={<HeaderLinks />}
            // {...rest}
            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={4}>
                            <Card className={classes[cardAnimaton]}>
                                <Formik
                                    initialValues={{ email: '', password: '' }}
                                    onSubmit={(values, { setStatus, setSubmitting }) => onSubmit(values, { setStatus, setSubmitting })}
                                >
                                    {({ values, status, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                                        <form noValidate={true} onSubmit={handleSubmit}>
                                            <CardHeader color="primary" className={classes.cardHeader}>
                                                <h4>Login</h4>
                                                <div className={classes.socialLine}>
                                                    <Button
                                                        justIcon
                                                        href="#pablo"
                                                        target="_blank"
                                                        color="transparent"
                                                        onClick={e => e.preventDefault()}
                                                    >
                                                        <i className={"fab fa-twitter"} />
                                                    </Button>
                                                    <Button
                                                        justIcon
                                                        href="#pablo"
                                                        target="_blank"
                                                        color="transparent"
                                                        onClick={e => e.preventDefault()}
                                                    >
                                                        <i className={"fab fa-facebook"} />
                                                    </Button>
                                                    <Button
                                                        justIcon
                                                        href="#pablo"
                                                        target="_blank"
                                                        color="transparent"
                                                        onClick={e => e.preventDefault()}
                                                    >
                                                        <i className={"fab fa-google-plus-g"} />
                                                    </Button>
                                                </div>
                                            </CardHeader>
                                            <p className={classes.divider}>Or Be Classical</p>
                                            <CardBody>
                                                <TextField
                                                    className={classesRoot.root}
                                                    type='email'
                                                    label='Email'
                                                    margin='normal'
                                                    name='email'
                                                    onBlur={handleBlur}
                                                    onChange={handleChange}
                                                    value={values.email}
                                                />
                                                <TextField
                                                    className={classesRoot.root}
                                                    type='password'
                                                    label='Password'
                                                    margin='normal'
                                                    name='password'
                                                    onBlur={handleBlur}
                                                    onChange={handleChange}
                                                    value={values.password}
                                                />
                                            </CardBody>
                                            <CardFooter className={classes.cardFooter}>
                                                <Button simple color='primary' size="lg" type='submit' disabled={isSubmitting}>
                                                    Login
                                                </Button>
                                            </CardFooter>
                                        </form>
                                    )}
                                </Formik>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}

const mapDispatchToProps = dispatch => ({
    login: (requestType, params) => dispatch(authActions.loginRequest(requestType, params)),
});

export default connect(null, mapDispatchToProps)(LoginPage);