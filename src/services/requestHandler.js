import axios from 'axios';
import { getAuthToken } from '../helpers/authHeader';

class ApiRequestHandler {

    async handleRequest(method, url, body, headers, options) {
        try {
            let validToken = null, { unAuth, noTimeout } = options || {};
            let isUpload = headers && headers['Content-Type'] === 'multipart/form-data';
            if (!unAuth) {
                validToken = await getAuthToken();
                if (validToken) {
                    headers.Authorization = validToken;
                }
                return Promise.reject({ message: 'Back to login screens' })
            }
            let reqConfig = {
                url, method, headers,
                timeout: noTimeout ? 0 : 10000,
                data: !isUpload && body ? JSON.stringify(body) : (body || '{}'),
            }
            const result = await axios(reqConfig);
            return Promise.resolve(result.data)
        } catch (e) {
            if (e && e.response && e.response.data === 'Unauthorized')
                return Promise.reject({ message: 'Unauthorized' })
            return Promise.reject(e)
        }
    }
}

export default ApiRequestHandler;